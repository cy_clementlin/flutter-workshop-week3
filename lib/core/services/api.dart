
import 'package:dio/dio.dart';
import 'package:flutter_workshop_week3/core/models/pokemon.dart';
import 'package:flutter_workshop_week3/core/models/pokemon_info.dart';

class Api {
  static const endpoint = 'https://pokeapi.co/api/v2';

  static const _POKEMON = '/pokemon';

  Dio _dio = Dio(BaseOptions(baseUrl: endpoint),);

  ///
  /// ==== dio example ====
  /// dio source: https://pub.dev/packages/dio
  /// ex:
  ///   Response response = awiat dio.get(API_PATH);
  ///   print(response.data.toString());
  ///

  /// === Pokemon data api ===
  /// || https://pokeapi.co/ ||
  /// ========================
  ///
  Future<List<PokeMon>> fetchPokeMons() {
    throw UnimplementedError('do it');
  }

  Future<PokeMonInfo> fetchPokeMonInfoById(int id) {

    throw UnimplementedError('do it');
  }

}